import argparse
from robohash import Robohash


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--set", default="set1")
    parser.add_argument("-x", "--width", type=int, default=300)
    parser.add_argument("-y", "--height", type=int, default=300)
    parser.add_argument("-o", "--output", default="robohash.png")
    parser.add_argument("text", help="Text to use for the hash")

    args = parser.parse_args()

    robohash = Robohash(args.text)
    robohash.assemble(roboset=args.set)

    robohash.img.save(args.output)


if __name__ == "__main__":
    main()
